package fs

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type Directory struct {
	Name     string
	Path     string
	Contents map[string]*FileInfo
	Parent   string
}

func NewDirectory() *Directory {
	return &Directory{
		Name:     "",
		Path:     "",
		Contents: make(map[string]*FileInfo),
		Parent:   "",
	}
}

type DataStore struct {
	Root map[string]*Directory
	Path string
	Enc  Encoder
}

//NewStore create new datastore
func NewStore(key string) *DataStore {
	d := &DataStore{
		Root: make(map[string]*Directory),
		Path: "./",
		Enc:  NewEncoder(),
	}
	d.Enc.WithKey(key)
	return d
}

func (d *DataStore) RootPath(path string) (*Directory, error) {
	root, name := filepath.Split(path)
	d.Path = root

	dir := NewDirectory()
	dir.Name = name
	dir.Path = path
	dir.Parent = dir.Name
	d.Root[dir.Name] = dir

	return d.Root[dir.Name], nil
}

func (d *DataStore) AddDirectory(parent, path string) (*Directory, error) {

	//makes all paths forward slash, real path may be windows: \path\to\file.ext
	fmtPath := stdpathname(path)

	if d.Root[fmtPath] != nil {
		return d.Root[fmtPath], errors.New("directory already exsists")
	}

	dir := NewDirectory()
	//store real path
	_, name := filepath.Split(path)
	dir.Name = fmtPath
	dir.Path = d.Path + path
	dir.Parent = parent

	//create new Directory to store elements
	d.Root[fmtPath] = dir

	//create reference in parent Directory
	fi := NewFileInfo()
	fi.name = name
	fi.Path = d.Path + path
	fi.Parent = parent
	fi.dir = true

	//create directory entry on parent
	d.Root[parent].Contents[name] = fi

	return d.Root[fmtPath], nil
}

func (d *DataStore) InfoFromPath(path string) error {
	err := d.infoAll(path)
	if err != nil {
		return err
	}
	return nil
}

func (d *DataStore) GetDirInfo(path string) ([]os.FileInfo, error) {

	out := make([]os.FileInfo, 0)
	path = stdpathname(path)

	for _, v := range d.Root[path].Contents {
		out = append(out, v)
	}

	return out, nil
}

func (d *DataStore) GetContents(name string) (*FileInfo, error) {

	//todo: clean up
	defaultInfo := NewFileInfo()
	dir, filename := formatPath(name)
	isDir := true
	dirName := dir + "/" + filename
	dirName = strings.TrimLeft(dirName, `/`)

	if len(strings.Split(dirName, ".")) > 1 {
		isDir = false
	}
	if isDir {

		if d.Root[dirName] == nil {
			return defaultInfo, os.ErrNotExist
		}

		if d.Root[dirName] != nil {
			if d.Root[dirName].Contents[dirName] != nil {
				return d.Root[dirName].Contents[dirName], nil
			}
			//handle root
			root := NewFileInfo()
			root.name = dirName
			root.dir = true
			root.Path = dirName
			return root, nil
		}

		if d.Root[dirName].Contents[dirName] != nil {

			return d.Root[dirName].Contents[dirName], nil
		}
		return defaultInfo, os.ErrNotExist
	}

	//handle resource
	if d.Root[dir] == nil {

		return defaultInfo, os.ErrNotExist
	}

	if d.Root[dir].Contents[filename] != nil {

		//resolve file
		var buf bytes.Buffer

		encfile := d.Root[dir].Contents[filename]
		loc := encfile.Path

		ef, err := os.Open(loc)
		if err != nil {
			return defaultInfo, os.ErrNotExist
		}

		b, err := ioutil.ReadAll(ef)
		if err != nil {
			return defaultInfo, os.ErrNotExist
		}
		err = d.Enc.Decode(bytes.NewReader(b), &buf)
		if err != nil {
			log.Println(err)
		}

		bc := make([]byte, buf.Len())
		copy(bc, buf.Bytes())

		buf.Reset()
		err = Unzip(bytes.NewReader(bc), &buf)
		if err != nil {
			log.Println(err)
		}

		d.Root[dir].Contents[filename].Bytes = buf.Bytes()

		return d.Root[dir].Contents[filename], nil
	}

	return defaultInfo, io.EOF
}

func (d *DataStore) infoAll(dir string) error {
	parent, _ := d.RootPath(dir)
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		finfo := newFileInfoFromOS(info)
		finfo.Parent = parent.Name
		finfo.Path = d.Path + path

		if info.IsDir() {
			direct, err := d.AddDirectory(parent.Name, path)
			if err != nil {
				//directory already exists
				//continue
				return nil
			}
			parent = direct
			return nil
		}
		parent.Contents[finfo.name] = finfo
		return nil
	})
	return err
}

func newFileInfoFromOS(info os.FileInfo) *FileInfo {
	finfo := NewFileInfo()

	finfo.name = info.Name()
	finfo.size = info.Size()
	finfo.dir = info.IsDir()
	finfo.modtime = info.ModTime()
	finfo.mode = info.Mode()
	return finfo
}

func formatPath(path string) (string, string) {
	path = strings.TrimLeft(path, `/`)
	path = strings.TrimLeft(path, `\`)

	dir, filename := filepath.Split(path)
	dir = strings.Replace(dir, `\`, `/`, -1)
	dir = strings.TrimRight(dir, `/`)

	return dir, filename
}

func readSingleBytes(f io.Reader) ([]byte, error) {
	out := make([]byte, 0)
	buf := make([]byte, 1)
	for {
		_, err := f.Read(buf)
		if err != nil {
			if err != io.EOF {
				return nil, err
			}
			break
		}
		out = append(out, buf...)
	}
	return out, nil
}

func stdpathname(dir string) string {

	dir = strings.TrimLeft(dir, `.`)
	dir = strings.Replace(dir, `\`, `/`, -1)
	dir = strings.TrimLeft(dir, `/`)
	dir = strings.TrimRight(dir, `/`)
	return dir
}
