package fs

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io/ioutil"

	mrand "math/rand"
	"strings"
	"time"

	"encoding/hex"
	"errors"
	"io"
)

//Encoder encoded and decodes data
type Encoder interface {
	//Encode a io.Reader contents and save encoded bytes to destination io.Writer
	Encode(src io.Reader, dst io.Writer) error
	//Read contents and save decoded to destination io.Writer
	Decode(src io.Reader, dst io.Writer) error
	//WithKey adds key to Encoder
	WithKey(string)
	//GetKey gets key from Encoder
	GetKey() string
}

type EncEncoder struct {
	key string
}

func NewEncoder() *EncEncoder {
	seed()
	return &EncEncoder{
		key: GenKey(64),
	}
}

func (e *EncEncoder) Encode(src io.Reader, dst io.Writer) error {
	rawbytes, err := ioutil.ReadAll(src)
	if err != nil {
		return err
	}
	data, err := EncodeBytes(rawbytes, e.key)
	if err != nil {
		return err
	}
	_, err = dst.Write(data)
	return err
}

func (e *EncEncoder) Decode(src io.Reader, dst io.Writer) error {
	rawbytes, err := ioutil.ReadAll(src)
	if err != nil {
		return err
	}
	data, err := DecodeBytes(rawbytes, e.key)
	if err != nil {
		return err
	}
	_, err = dst.Write(data)
	return err
}

func (e *EncEncoder) GetKey() string {
	return e.key
}

func (e *EncEncoder) WithKey(k string) {
	e.key = k
}

func seed() {
	mrand.Seed(time.Now().UnixNano())
}

func GenKey(n int) string {
	// 6368616e676520746869732070617373
	r := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "e", "f"}
	out := make([]string, 0)
	for i := 0; i < n; i++ {
		out = append(out, r[mrand.Intn(15)])
	}
	return strings.Join(out, "")
}

func NewKey(secret string) []byte {
	key, _ := hex.DecodeString(secret)
	return key
}

func EncodeBytes(b []byte, secret string) ([]byte, error) {

	key, err := hex.DecodeString(secret)
	if err != nil {
		return b, err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return b, err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return b, err
	}
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], b)
	return ciphertext, nil

}

func DecodeBytes(b []byte, secret string) ([]byte, error) {

	key, _ := hex.DecodeString(secret)
	ciphertext := b

	block, err := aes.NewCipher(key)
	if err != nil {
		return b, err
	}
	if len(ciphertext) < aes.BlockSize {
		return b, errors.New("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)
	return ciphertext, nil
}
