package main

import (
	"context"
	"flag"
	"log"
	"os"
	"time"

	"github.com/webview/webview"
	"gitlab.com/ds-go/skeleton/crypto"
	"gitlab.com/krobolt/go-data/fs"
)

var (
	dev      = false
	httpPath = fs.GenKey(32)
	port     = "9097"
)

func main() {

	flag.BoolVar(&dev, "dev", false, "enable dev mode")
	flag.Parse()

	wsManager = NewWSSessionManager()

	shutdown := make(chan int)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	go serveWSS()
	view()

	go func() {
		if err := srv.Shutdown(ctx); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		}
		log.Println("shut down webserver")
		shutdown <- 1
	}()

	cancel()

	//wait for shutdown
	<-shutdown

}

func view() {

	w := webview.New(dev)

	defer w.Destroy()

	w.SetTitle("DSPass Secure Password Storage")
	w.SetSize(640, 480, webview.HintNone)

	w.Bind("HashPassword", func(a string) string {
		b, err := crypto.HashPassword(a, 15)
		if err != nil {
			return ""
		}
		return string(b)
	})

	w.Bind("GetKey", func(a string) string {
		f, err := os.Open(a + fileext)
		if err != nil {
			return ""
		}
		defer f.Close()
		return GetFileHeader(f)
	})

	w.Bind("CheckUser", func(a, b string) string {
		_, _, err := ReadPasswordFile(a, b)
		if err != nil {
			return "unable to verify user"
		}
		return ""
	})

	w.Bind("VerifyPassword", func(a, b string) bool {
		return crypto.VerifyPassword(a, b)
	})

	w.Bind("CreateProfile", func(a, b string) string {
		key, err := CreateNewPasswordFile(a, b)
		if err != nil {
			return ""
		}
		return key
	})

	w.Bind("GetPasswords", func(a, b string) string {
		_, data, err := ReadPasswordFile(a, b)
		if err != nil {
			return ""
		}
		return data
	})

	w.Bind("SavePasswords", func(a, b, d string) {
		UpdatePasswordFile(a, b, d)
	})

	w.Bind("quit", func() {
		w.Terminate()
	})

	//w.Navigate(`http://localhost:9097/hello` + port + "/" + httpPath)
	w.Navigate(`http://localhost:` + port + "/" + httpPath)
	w.Run()
	log.Println("closed application")
}

func TestWindow() {

	w := webview.New(dev)

	defer w.Destroy()

	w.SetTitle("Minimal webview example")
	w.SetSize(800, 600, webview.HintNone)

	w.Navigate(`data:text/html,
		<!doctype html>
		<html>
			<body>hello</body>
			<script>
				window.onload = function() {
					document.body.innerText = ` + "`hello, ${navigator.userAgent}`" + `;
					noop().then(function(res) {
						console.log('noop res', res);
						add(1, 2).then(function(res) {
							console.log('add res', res);
							quit();
						});
					});
				};
			</script>
		</html>
	)`)
	w.Run()
}
