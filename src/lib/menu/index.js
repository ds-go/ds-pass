require('./_menu.scss?inline');

import {NewMenu,LinuxMenuItems,MacMenuItems} from "./menu"

function NewLinuxMenu(store){
    return NewMenu(
        store,
        LinuxMenuItems,
        'Some title for this project'
    );
}

function NewMacMenu(){
    return NewMenu(
        MacMenuItems,
        'Some title for this project'
    );
}

function LoadAppMenu(platform){
    var menuarea = document.getElementById("menuarea")
    switch(platform){
        case "darwin":
            menuarea.appendChild(NewMacMenu())           
            break;    
        default:
            menuarea.appendChild(NewLinuxMenu())
    }
}

class Menu {
    constructor(store) {
        menuarea.appendChild(NewLinuxMenu(store))
    }
}

export {Menu as default};