export function MinControlBtn(){
    let min = _ListItem("–","min-btn")
    min.addEventListener("click", (e) => {
        e.preventDefault();
     
        window.minimize(); 
    });
    min.addEventListener('mouseover',() => {
        min.classList.add('active');
    })
    min.addEventListener('mouseout',() => {
        min.classList.remove('active');
    })
    return min
}

export function ToggleDocMax(){
   
    document.body.classList.toggle('maxed')
    if (document.body.classList.contains('maxed')){
        window.maximize();          
    }else{
        window.unmaximize();
    }
}

//Create Max Controls
export function MaxControlBtn(){
    let max = _ListItem("□","max-btn")
    max.addEventListener("click", (e) => {
        e.preventDefault();
     
        document.body.classList.toggle('maxed')
        if (document.body.classList.contains('maxed')){
            window.maximize();          
        }else{
            window.unmaximize();
        }
    });
    return max
}

//Create Close Controls
export function CloseControlBtn(){
    let close = _ListItem("✕","close-btn")    
    close.addEventListener("click", (e) => {
        e.preventDefault();
        //var window = remote.getCurrentWindow();
        window.close();
    }); 
    return close
}


function _ListItem(html, id){
    let l = document.createElement('li');   
    l.innerHTML = html
    l.setAttribute('id',id)
    return l
}
