import { connect } from 'react-redux'
import App from '../../components/app/app';

import {passwordSlice} from "../slices/password"
import {appSlice} from '../slices/appSlice';

class AppRedux extends App {}

const mapStateToProps = state => ({
  app: state.app,
  passwords: state.passwords,
})

const mapDispatchToProps = {
  ...appSlice.actions,
  ...passwordSlice.actions,
}

export default connect(mapStateToProps, mapDispatchToProps)(AppRedux);
