import { ADD_GAME } from "./types"

const initialState = {
    games: [
      { title: "I love redux" },
      { title: "The redux song" },
      { title: "Run to the redux hill" },
    ],
  }
  
  export default function(state = initialState, action) {
    switch(action.type) {
      case ADD_GAME:
        return {
          games: [action.payload, ...state.games]
        }
      default:
        return state;
      }
  }