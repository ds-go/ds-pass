import { createSlice } from '@reduxjs/toolkit'

export const passwordSlice = createSlice({  
    name: 'passwords',  
    initialState: {    
        list: [],
    },
    reducers: {        
        withPasswordList: (state, action) => {   
            state.list = action.payload.Passwords                                      
        },           
        savePassword: (state, action) => { 
            var pwobj = {
                Username: action.payload.Username,
                Description: action.payload.Description,
                Password: action.payload.Password,
            }           
            state.list.push(pwobj)                
        },          
    },
})

export default passwordSlice.reducer