export const WebpackDevServerPolys = () => {
  if (!window.CreateProfile){
    window.CreateProfile = async (key, hash) =>{
      console.log("Webpack polyfill, golang injected function decode password:",key, hash)
      return
    }  
  }
  if (!window.HashPassword){
    window.HashPassword = async (key) =>{
      console.log("Webpack polyfill, golang injected function hash password:",key)
      return key
    }  
  }
  if (!window.GetPasswords){
    window.GetPasswords = async (user, pwd) =>{
      console.log("Webpack polyfill, golang injected function get passwords:",user)
      return '{"User":"default","Passwords":[]}'
    }  
  }
  if (!window.SavePasswords){
    window.SavePasswords = async (user, pwd) =>{
      console.log("Webpack polyfill, golang injected function save passwords:",user)
      return
    }  
  }
  if (!window.GetKey){
    window.GetKey = async (user) =>{
      console.log("Webpack polyfill, golang injected function save passwords:",user)
      return user
    }  
  }
  if (!window.VerifyPassword){
    window.VerifyPassword = async (a,b) =>{
      console.log("Webpack polyfill, golang injected verify passwords:",a)
      return true
    }  
  }
}


