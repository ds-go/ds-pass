require('./_pin.scss?inline');

import React, { Component } from 'react';

class Pin extends Component {  

  constructor(props) {
    super(props);

    this.p1 = React.createRef();
    this.p2 = React.createRef();
    this.p3 = React.createRef();
    this.p4 = React.createRef();

    this.state = {
      p1 : "0",
      p2 : "0",
      p3 : "0",
      p4 : "0",
    }    
  }

  componentDidMount(){
    if (this.props.focus){
      this.p1.current.focus();
    }    
  }

  changep1(e){
    this.setState({
      p1: e.target.value,
    })
    if (e.target.value == ""){
      return
    }
    this.p2.current.focus()
  }

  changep2(e){
    this.setState({
      p2: e.target.value,
    })
    if (e.target.value == ""){
      return
    }
    this.p3.current.focus()
  }

  changep3(e){
    this.setState({
      p3: e.target.value,
    })
    if (e.target.value == ""){
      return
    }
    this.p4.current.focus()    
  }

  changep4(e){
    this.setState({
      p4: e.target.value,
    })    
  }

  clear(){
    this.setState({
      p1 : "0",
      p2 : "0",
      p3 : "0",
      p4 : "0",
    })
    this.p1.current.value = ""
    this.p2.current.value = ""
    this.p3.current.value = ""
    this.p4.current.value = ""
  }

  render() {
    return (    
      <React.Fragment>
        <input type="hidden" pattern="[0-9]{4}" value={this.state.p1+this.state.p2+this.state.p3+this.state.p4} readOnly/>
        <div className="Pin">          
          <input ref={this.p1} type="password" name="p1" maxLength="1" pattern="[0-9]{1}" onChange={this.changep1.bind(this)}/>
          <input ref={this.p2} type="password" name="p2" maxLength="1" pattern="[0-9]{1}" onChange={this.changep2.bind(this)}/>
          <input ref={this.p3} type="password" name="p3" maxLength="1" pattern="[0-9]{1}" onChange={this.changep3.bind(this)}/>
          <input ref={this.p4} type="password" name="p4" maxLength="1" pattern="[0-9]{1}" onChange={this.changep4.bind(this)}/>      
          <a onClick={this.clear.bind(this)}>clear</a>
        </div>
      </React.Fragment>     
    ); 
  }
}

export default Pin


