require('./_passwords.scss?inline');

import React, { Component } from 'react';

import Password from './password';

const PWREADY  = "ready"

class Passwords extends Component {  

  constructor(props) {
    super(props);
    this.state = {
      view: props.view,         
    }
  }

  async componentDidMount(){
    console.log("MOUNTED LIST")
    await this.props.updateList()
    this.isReady()
  }

  isReady(){   
    this.setState({
      view: PWREADY
    })
  } 


  map(children){
    if (!children || children.length == 0){
      return false
    } 
    return children.map((v,i) => {
      let c = children[i]
      return (
        <React.Fragment key={i+c.Description}>
          <Password 
          remove={this.props.remove}
          secret={this.props.secret}
          update={this.props.update}
          data={c}          
          ></Password>
        </React.Fragment>           
      );
    });     
  }
   
  render() {

    switch (this.state.view){
    
    case PWREADY:
        let items = this.map(this.props.items)      
        if (!items || items.length == 0){
            items = (
              <div className="Window">
                <h2>You do not have any passwords saved</h2>
                <p>Add a new password below.</p>
              </div>
            )
        }      
        return (
            <div className="Passwords">
                {items}
            </div>
        );
        
    default:      
        return (
           <div className="Passwords">
                <h1>Loading Passwords</h1>
          </div>
        );  
    }
      
  }
}

export default Passwords


