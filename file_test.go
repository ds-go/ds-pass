package main

import (
	"bytes"
	"testing"
)

func Test_CreateFileHeaderSizeAndVersion(t *testing.T) {

	expected := make([]byte, 512)
	key := []byte("key")
	expected[0] = byte(1)
	expected[1] = byte(len(key))

	actual := CreateFileHeader("key")
	if len(actual) != 512 {
		t.Error("wrong header len")
	}

	if actual[0] != expected[0] {
		t.Error("match failed")
	}

	if actual[1] != expected[1] {
		t.Error("match failed")
	}
}

func Test_GetFileHeader(t *testing.T) {
	expected := "897a7809708e789c70897809789a7089709d890987897098708978"
	header := CreateFileHeader(expected)
	br := bytes.NewReader(header)
	actual := GetFileHeader(br)
	if actual != expected {
		t.Error("failed to fetch header")
	}
}
